/**
 * Created by yzhb363 on 26/04/2017.
 */
"use script";

//background
$("input[type=radio]").each(function (i, obj) {
    $(this).click(function () {
        if (i + 1 == 6) {
            $("#background").attr("src", "../images/ex4/background" + (i + 1) + ".gif");
        } else {
            $("#background").attr("src", "../images/ex4/background" + (i + 1) + ".jpg");
        }
    });
});



//change creatures
$("input[type=checkbox]").each(function (i, checkbox) {

    var dolphinId = $(checkbox).attr("id");

    dolphinId = "#" + dolphinId.substring((dolphinId.indexOf('-') + 1));

    if (checkbox.checked) {
        $(dolphinId).css("visibility", "visible");
    } else {
        $(dolphinId).css("visibility", "hidden");
    }

    $(checkbox).change(function () {

        if (checkbox.checked) {
            $(dolphinId).css("visibility", "visible");
        } else {
            $(dolphinId).css("visibility", "hidden");
        }

    });
});

// slider bar
var originalSize = $(".dolphin").height();

$( function() {
    $("#size-control").slider({
        range: "max",
        min: 1,
        max: 10,
        value: 5,
        slide: function (event, ui) {
            $("#amount").val(ui.value);
            $(".dolphin").height(originalSize * (1 + ui.value / 5));
        }
    });
    $("#amount").val($("#size-control").slider("value"));
} );


